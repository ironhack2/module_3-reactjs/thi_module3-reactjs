import { Container, Grid, Alert, Box, Button, Input, MenuItem, Modal, Select, Snackbar, Typography, FormControl, InputLabel } from "@mui/material"
import { useEffect, useState } from "react";

function ModalEditOrder({ openModalEdit, handleCloseEdit, style, getData, setVarRefeshPage, varRefeshPage, rowClicked
}) {
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalEdit, setStatusModalEdit] = useState("error")
    const [noidungAlertValid, setNoidungAlertValid] = useState("")

    const [idEdit, setIdEdit] = useState("");
    const [orderId, setOrderId] = useState("");
    const [kichCo, setKichCo] = useState("");
    const [duongKinh, setDuongKinh] = useState("");
    const [suon, setSuon] = useState("");
    const [salad, setSalad] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("");

    const [voucher, setVoucher] = useState("");
    const [thanhTien, setThanhTien] = useState("");
    const [giamGia, setGiamGia] = useState("");
    const [loaiNuocUong, setLoaiNuocUong] = useState("");
    const [soLuongNuoc, setSoLuongNuoc] = useState("");

    const [hoTen, setHoten] = useState("");
    const [email, setEmail] = useState("");
    const [diaChi, setDiaChi] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [loiNhan, setLoiNhan] = useState("");
    const [status, setStatus] = useState("");

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseEdit()
    }


    const onBtnUpdateClick = () => {
        console.log("Update được click!")
        var vCheckData = validateDataForm()
        if (vCheckData) {
            const body = {
                method: 'PUT',
                body: JSON.stringify({
                    trangThai: status,
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            }

            getData("http://42.115.221.44:8080/devcamp-pizza365/orders/" + idEdit, body)
                .then((data) => {
                    console.log(data);
                    setOpenAlert(true);
                    setNoidungAlertValid("Update Order thành công!")
                    setStatusModalEdit("success")
                    setVarRefeshPage(varRefeshPage + 1)
                    handleCloseEdit()
                })
                .catch((error) => {
                    console.log(error);
                    setOpenAlert(true);
                    setNoidungAlertValid("Update User thất bại!")
                    setStatusModalEdit("error")
                    handleCloseEdit()
                })
        }
    }


    const validateDataForm = () => {
        if (!(status === "open" || status === "cancel" || status === "confirmed") || status === null) {
            setOpenAlert(true);
            setNoidungAlertValid("Trạng thái không đúng!")
            setStatusModalEdit("error")
            return false
        }
        else {
            return true
        }
    }

    useEffect(() => {
        setIdEdit(rowClicked.id)
        setOrderId(rowClicked.orderId)
        setKichCo(rowClicked.kichCo)
        setDuongKinh(rowClicked.duongKinh)
        setSuon(rowClicked.suon)
        setSalad(rowClicked.salad)
        setLoaiPizza(rowClicked.loaiPizza)
        setVoucher(rowClicked.idVourcher)
        setThanhTien(rowClicked.thanhTien)
        setGiamGia(rowClicked.giamGia)
        setLoaiNuocUong(rowClicked.idLoaiNuocUong)
        setSoLuongNuoc(rowClicked.soLuongNuoc)
        setHoten(rowClicked.hoTen)
        setEmail(rowClicked.email)
        setSoDienThoai(rowClicked.soDienThoai)
        setDiaChi(rowClicked.diaChi)
        setLoiNhan(rowClicked.loiNhan)
        setStatus(rowClicked.trangThai)
    }, [openModalEdit]);



    const onChangeIdEdit = (event) => {
        setIdEdit(event.target.value);
    }

    const onChangeOrderId = (event) => {
        setOrderId(event.target.value)
    }

    const onChangeKichCo = (event) => {
        setKichCo(event.target.value)
    }

    const onChangeDuongKinh = (event) => {
        setDuongKinh(event.target.value)
    }

    const onChangeSuon = (event) => {
        setSuon(event.target.value)
    }

    const onChangeSalad = (event) => {
        setSalad(event.target.value)
    }

    const onChangeLoaiPizza = (event) => {
        setLoaiPizza(event.target.value)
    }

    const onChangeVoucher = (event) => {
        setLoaiPizza(event.target.value)
    }

    const onChangeThanhTien = (event) => {
        setThanhTien(event.target.value)
    }

    const onChangeGiamGia = (event) => {
        setGiamGia(event.target.value)
    }

    const onChangeLoaiNuocUong = (event) => {
        setLoaiNuocUong(event.target.value)
    }

    const onChangeSoLuongNuoc = (event) => {
        setSoLuongNuoc(event.target.value)
    }

    const onChangeHoten = (event) => {
        setHoten(event.target.value)
    }

    const onChangeEmail = (event) => {
        setEmail(event.target.value)
    }

    const onChangeDiaChi = (event) => {
        setDiaChi(event.target.value)
    }

    const onChangeSoDienThoai = (event) => {
        setSoDienThoai(event.target.value)
    }

    const onChangeLoiNhan = (event) => {
        setLoiNhan(event.target.value)
    }

    const onSelectStatusChange = (event) => {
        setStatus(event.target.value)
    }

    return (
        <Container>
            <Modal
                open={openModalEdit}
                onClose={onBtnCancelClick}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} style={{ backgroundColor: "#e1f5fe" }}>
                    <Typography align="center" mb={2} id="modal-modal-title" variant="h4" style={{ color: "#0277bd" }}>
                        <b>Edit Order!!!</b>
                    </Typography>
                    <Grid container mt={5}>
                        <Grid item sm={6}>
                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>ID:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={idEdit ? idEdit : ""} onChange={onChangeIdEdit} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>orderId:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={orderId ? orderId : ""} onChange={onChangeOrderId} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Kích Cỡ:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={kichCo ? kichCo : ""} onChange={onChangeKichCo} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Đường kính:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={duongKinh ? duongKinh : ""} onChange={onChangeDuongKinh} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Sườn:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={suon ? suon : ""} onChange={onChangeSuon} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Salad:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={salad ? salad : ""} onChange={onChangeSalad} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Loại pizza:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={loaiPizza ? loaiPizza : ""} onChange={onChangeLoaiPizza} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Voucher:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={voucher ? voucher : ""} onChange={onChangeVoucher} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Thành  tiền:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={thanhTien ? thanhTien : ""} onChange={onChangeThanhTien} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>


                        <Grid item sm={6}>
                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Giảm giá:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={giamGia ? giamGia : ""} onChange={onChangeGiamGia} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Loại nước uống:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={loaiNuocUong ? loaiNuocUong : ""} onChange={onChangeLoaiNuocUong} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Số lượng nước uống:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={soLuongNuoc ? soLuongNuoc : ""} onChange={onChangeSoLuongNuoc} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Khách hàng:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={hoTen ? hoTen : ""} onChange={onChangeHoten} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Email:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={email ? email : ""} onChange={onChangeEmail} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Số điện thoại:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={soDienThoai ? soDienThoai : ""} onChange={onChangeSoDienThoai} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Địa chỉ:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={diaChi ? diaChi : ""} onChange={onChangeDiaChi} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container>
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Lời nhắn:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#f5f5f5" }} value={loiNhan ? loiNhan : ""} onChange={onChangeLoiNhan} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container className="mt-3">
                                <Grid item sm={12} mt={1}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label> Trạng thái:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <FormControl sx={{ width: 200 }}>
                                                <InputLabel id="demo-simple-select-helper-label">Trạng Thái</InputLabel>
                                                <Select
                                                    id="registerstatus-select"
                                                    value={status ? status : ""}
                                                    fullWidth
                                                    label="Trạng thái"
                                                    onChange={onSelectStatusChange}
                                                >
                                                    <MenuItem value="open">open</MenuItem>
                                                    <MenuItem value="cancel">cancel</MenuItem>
                                                    <MenuItem value="confirmed">confirmed</MenuItem>
                                                </Select>
                                            </FormControl>

                                            {!(status === "open" || status === "cancel" || status === "confirmed") ? <i style={{ fontSize: "10px", color: "red" }}> Status không nằm trong select dữ liệu!</i> : null}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid container className="mt-4 text-center">
                        <Grid item sm={12} mt={1}>
                            <Grid container mt={4}>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnUpdateClick} className="bg-primary w-100 text-white">Update Order</Button>
                                </Grid>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnCancelClick} className="bg-secondary w-75 text-white">Hủy Bỏ</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModalEdit} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </Container>
    )
}
export default ModalEditOrder