import { Container, Grid, Alert, Box, Button, TextField, FormControl, InputLabel, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material";
import { useState, useEffect } from 'react';

const gMenuComboSize = [
    {
        menuName: "S",
        duongKinhCM: 20,
        suonNuong: 2,
        saladGr: 200,
        drink: 2,
        priceVND: 150000,
    },
    {
        menuName: "M",
        duongKinhCM: 25,
        suonNuong: 4,
        saladGr: 300,
        drink: 3,
        priceVND: 200000,
    },
    {
        menuName: "L",
        duongKinhCM: 30,
        suonNuong: 8,
        saladGr: 500,
        drink: 4,
        priceVND: 250000,
    },
]

function ModalAddNew({ openModalAdd, setOpenModalAdd, handleClose, style, getData, setVarRefeshPage, varRefeshPage }) {

    const [drinkSelect, setDrinkSelect] = useState([]);

    const [comboAddNewForm, setComboAddNew] = useState([]);

    const [duongKinhAddNew, setDuongKinhAddNew] = useState("");
    const [suonAddNew, setSuonAddNew] = useState("");
    const [salad, setSalad] = useState("");
    const [pizzaAddNew, setPizzaAddNew] = useState("");
    const [amountDrink, setAmountDrink] = useState("");
    const [priceAddNew, setPriceAddNew] = useState("");

    const [voucherAddNew, setVoucherAddNew] = useState("");
    const [discountAddNew, setDiscountAddNew] = useState("");

    const [drinkAddNew, setDrinkAddNew] = useState("");

    const [nameAddNew, setNameAddNew] = useState("");
    const [emailAddNew, setEmailAddNew] = useState("");
    const [phoneAddNew, setPhoneAddNew] = useState("");
    const [addressAddNew, setAddressAddNew] = useState("");
    const [messageAddNew, setMessageAddNew] = useState("");

    const [status, setStatus] = useState("");


    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("");
    const [statusModal, setStatusModal] = useState("error");



    useEffect((data) => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinkSelect(data);
            })
            .catch((error) => {
                console.error(error.message);
            })
    })

    useEffect(() => {
        if (voucherAddNew.length >= 5) {
            getData("http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + voucherAddNew)
                .then((data) => {
                    setDiscountAddNew(data.phanTramGiamGia + "%");
                })
                .catch((error) => {
                    setDiscountAddNew(0 + "%")
                    console.error(error.message);
                })
        } else {
            setDiscountAddNew(0 + "%")
        }
    }, [voucherAddNew])


    useEffect((data) => {
        for (let bI = 0; bI < gMenuComboSize.length; bI++) {
            if (comboAddNewForm === gMenuComboSize[bI].menuName) {
                setDuongKinhAddNew(gMenuComboSize[bI].duongKinhCM);
                setSuonAddNew(gMenuComboSize[bI].suonNuong);
                setSalad(gMenuComboSize[bI].saladGr);
                setAmountDrink(gMenuComboSize[bI].drink);
                setPriceAddNew(gMenuComboSize[bI].priceVND);
            }
        }
    })

    const onChangeCombo = (event) => {
        setComboAddNew(event.target.value);
    }

    const onChangeDuongKinh = (event) => {
        setDuongKinhAddNew(event.target.value);
    }

    const onChangeSuon = (event) => {
        setSuonAddNew(event.target.value);
    }

    const onChangeSalad = (event) => {
        setSalad(event.target.value)
    }

    const onChangePizza = (event) => {
        setPizzaAddNew(event.target.value);
    }

    const changeAmountDrink = (event) => {
        setAmountDrink(event.target.value);
    }

    const changePrice = (event) => {
        setPriceAddNew(event.target.value);
    }

    const changeVoucher = (event) => {
        setVoucherAddNew(event.target.value);
    }

    const changeDiscount = (event) => {
        setDiscountAddNew(event.target.value);
    }

    const changeDrink = (event) => {
        setDrinkAddNew(event.target.value);
    }

    const changeName = (event) => {
        setNameAddNew(event.target.value);
    }

    const changeEmail = (event) => {
        setEmailAddNew(event.target.value);
    }

    const changePhone = (event) => {
        setPhoneAddNew(event.target.value);
    }

    const changeAddress = (event) => {
        setAddressAddNew(event.target.value);
    }

    const changeMessage = (event) => {
        setMessageAddNew(event.target.value);
    }

    const changeStatus = (event) => {
        setStatus(event.target.value);
    }

    const onBtnInsertClick = () => {
        var vDataOrder = {
            kichCo: comboAddNewForm,
            duongKinh: duongKinhAddNew,
            suon: suonAddNew,
            salad: salad,
            soLuongNuoc: amountDrink,
            thanhTien: priceAddNew,
            loaiPizza: pizzaAddNew,
            idVourcher: voucherAddNew,
            idLoaiNuocUong: drinkAddNew,
            hoTen: nameAddNew,
            email: emailAddNew,
            soDienThoai: phoneAddNew,
            diaChi: addressAddNew,
            loiNhan: messageAddNew,
            trangThai: status
        }

        var vCheckForm = validateDataForm(vDataOrder)
        if (vCheckForm) {
            const body = {
                method: 'POST',
                body: JSON.stringify({
                    kichCo: vDataOrder.kichCo,
                    duongKinh: vDataOrder.duongKinh,
                    suon: vDataOrder.suon,
                    salad: vDataOrder.salad,
                    soLuongNuoc: vDataOrder.soLuongNuoc,
                    thanhTien: vDataOrder.thanhTien,
                    loaiPizza: vDataOrder.loaiPizza,
                    idVourcher: vDataOrder.idVourcher,
                    idLoaiNuocUong: vDataOrder.idLoaiNuocUong,
                    hoTen: vDataOrder.hoTen,
                    email: vDataOrder.email,
                    soDienThoai: vDataOrder.soDienThoai,
                    diaChi: vDataOrder.diaChi,
                    loiNhan: vDataOrder.loiNhan,
                    trangThai: vDataOrder.trangThai
                }),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData('http://42.115.221.44:8080/devcamp-pizza365/orders/', body)
                .then((data) => {
                    setOpenAlert(true);
                    setStatusModal("success")
                    setNoidungAlertValid("Dữ liệu thêm thành công!")
                    setOpenModalAdd(false)
                    setVarRefeshPage(varRefeshPage + 1);
                    console.log(data);
                    window.location.reload();
                })
                .catch((error) => {
                    setOpenAlert(true);
                    setStatusModal("error")
                    setNoidungAlertValid("Dữ liệu thêm thất bại!")
                })
        }
    }

    const validateDataForm = (paramDataOrder) => {
        if (paramDataOrder.duongKinh === "") {
            setOpenAlert(true);
            setNoidungAlertValid("Hãy chọn combo!")
            setStatusModal("error")
            return false
        }

        if (paramDataOrder.loaiPizza === "") {
            setOpenAlert(true);
            setNoidungAlertValid("Hãy chọn loại Pizza!")
            setStatusModal("error")
            return false
        }
        if (paramDataOrder.idLoaiNuocUong === "") {
            setOpenAlert(true);
            setNoidungAlertValid("Hãy điền chọn đồ uống!")
            setStatusModal("error")
            return false
        }
        if (paramDataOrder.hoTen === "") {
            setOpenAlert(true);
            setNoidungAlertValid("Hãy điền họ tên!")
            setStatusModal("error")
            return false
        }
        if (paramDataOrder.soDienThoai.length < 8) {
            setOpenAlert(true);
            setNoidungAlertValid("Số điện thoại không hợp lệ!")
            setStatusModal("error")
            return false
        }

        //Check Email
        const vREG = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!(vREG.test(paramDataOrder.email))) {
            setOpenAlert(true);
            setNoidungAlertValid("Email không hợp lệ!")
            setStatusModal("error")
            return false
        }
        else {
            return true
        }
    }


    const handelCloseAlert = () => {
        setOpenAlert(false);
    }


    const onBtnCancelClick = () => {
        handleClose()
    }

    return (
        <>
            <Modal
                open={openModalAdd}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style} style={{ backgroundColor: "#f1f8e9" }}>
                    <Typography id="modal-modal-title" variant="h4" align="center" style={{ color: "#00695c" }}>
                        <strong>Thêm Order</strong>
                    </Typography>

                    <Grid container style={{ marginTop: "50px" }}>
                        <Grid item sm={6}>
                            <Grid container>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Pizza Size:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <FormControl sx={{ width: 200 }}>
                                                <InputLabel>Chọn Combo Pizza</InputLabel>
                                                <Select
                                                    defaultValue="Not"
                                                    label="Chọn Combo Pizza"
                                                    onChange={onChangeCombo}
                                                >
                                                    <MenuItem value="Not">Chọn Combo Pizza</MenuItem>
                                                    <MenuItem value="S">Size S</MenuItem>
                                                    <MenuItem value="M">Size M</MenuItem>
                                                    <MenuItem value="L">Size L</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Đường kính:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={duongKinhAddNew} onChange={onChangeDuongKinh} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Sườn Nướng:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={suonAddNew} onChange={onChangeSuon} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Salad:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={salad} onChange={onChangeSalad} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Số lượng nước:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={amountDrink} onChange={changeAmountDrink} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Thành tiền:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={priceAddNew} onChange={changePrice} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Voucher:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField type="number" label="Voucher" style={{ width: "200px", backgroundColor: "#ffffff" }} className="bg-white" onChange={changeVoucher} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Giảm giá:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <Input readOnly style={{ backgroundColor: "#eeeeee" }} value={discountAddNew} onChange={changeDiscount} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={3}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Loại Pizza:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <FormControl sx={{ width: 200 }}>
                                                <InputLabel>Chọn Pizza</InputLabel>
                                                <Select
                                                    defaultValue="Not"
                                                    label="Chọn Pizza"
                                                    onChange={onChangePizza}
                                                >
                                                    <MenuItem value="Not">Chưa chọn Pizza</MenuItem>
                                                    <MenuItem value="Seafood">Hải sản</MenuItem>
                                                    <MenuItem value="Hawaii">Hawaii</MenuItem>
                                                    <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>

                        <Grid item sm={6}>
                            <Grid container >
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Đồ uống:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <FormControl sx={{ width: 200 }}>
                                                <InputLabel>Đồ uống</InputLabel>
                                                <Select
                                                    defaultValue="Not"
                                                    label="Đồ uống"
                                                    onChange={changeDrink}
                                                >
                                                    <MenuItem value="Not">Hãy chọn nước uống</MenuItem>
                                                    {
                                                        drinkSelect.map((element, index) => {
                                                            return (
                                                                <MenuItem key={index} value={element.maNuocUong}>{element.tenNuocUong}</MenuItem>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Họ tên:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField label="Full Name" style={{ width: "200px", backgroundColor: "#ffffff" }} value={nameAddNew} onChange={changeName} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Số điện thoại:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField type="number" label="Phone" style={{ width: "200px", backgroundColor: "#ffffff" }} value={phoneAddNew} onChange={changePhone} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Email:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField label="Email" style={{ width: "200px", backgroundColor: "#ffffff" }} value={emailAddNew} onChange={changeEmail} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Địa chỉ:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField label="Address" style={{ width: "200px", backgroundColor: "#ffffff" }} value={addressAddNew} onChange={changeAddress} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={1}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Lời nhắn:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <TextField label="Message" style={{ width: "200px", backgroundColor: "#ffffff" }} value={messageAddNew} onChange={changeMessage} />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            <Grid container mt={3}>
                                <Grid item sm={12}>
                                    <Grid container>
                                        <Grid item sm={5}>
                                            <label>Trạng thái:</label>
                                        </Grid>
                                        <Grid item sm={7}>
                                            <FormControl sx={{ width: 200 }}>
                                                <InputLabel>Trạng thái</InputLabel>
                                                <Select
                                                    defaultValue="Open"
                                                    label="Trạng thái"
                                                    onChange={changeStatus}
                                                    readOnly
                                                >
                                                    <MenuItem value="Open">Open</MenuItem>
                                                    {/* <MenuItem value="Confirmed">Confirmed</MenuItem>
                                                    <MenuItem value="Cancel">Cancel</MenuItem> */}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid container className="mt-4 text-center">
                        <Grid item sm={12}>
                            <Grid container className="mt-4">
                                <Grid item sm={6}>
                                    <Button onClick={onBtnInsertClick} className="bg-success w-75 text-white">Insert Order</Button>
                                </Grid>
                                <Grid item sm={6}>
                                    <Button onClick={onBtnCancelClick} className="bg-secondary w-75 text-white">Hủy Bỏ</Button>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handelCloseAlert}>
                <Alert onClose={handelCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default ModalAddNew;