import { Button, InputLabel, MenuItem, Select, Pagination, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import { useEffect, useState } from 'react';

import ModalAddNew from "./ModalAddNew";
import ModalEditOrder from "./ModalEditOrder";
import ModalDeleteOrder from "./ModalDeleteOrder";


function OrderTable() {
    //Style
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 900,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
        fontWeight: "bold",
    };

    //PANIGATION
    //Limit: số lượng bản ghi trên 1 trang
    const [limit, setLimit] = useState(10);
    //số trang: tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
    const [noPage, setNoPage] = useState(0);
    //Trang hiện tại
    const [page, setPage] = useState(1);


    //Data row
    const [rowOrder, setRowOrder] = useState([]);

    //Thêm Row mới
    const [rowClicked, setRowClicked] = useState([]);

    //Load trang
    const [varRefeshPage, setVarRefeshPage] = useState(0);

    //Mở Modal
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [openModalEdit, setOpenModalEdit] = useState(false);
    const [openModalDelete, setOpenModalDelete] = useState(false);

    const [idDelete, setIdDelete] = useState("");

    //Đóng Modal
    const handleClose = () => setOpenModalAdd(false);
    const handleCloseEdit = () => setOpenModalEdit(false);
    const handleCloseDelete = () => setOpenModalDelete(false);


    //Call API
    const getData = async (paramUrl, paramBody = {}) => {
        const response = await fetch(paramUrl, paramBody);
        const responseData = await response.json();
        return responseData;
    }

    useEffect(() => {
        getData("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);

                setNoPage(Math.ceil(data.length / limit));
                setRowOrder(data.slice((page - 1) * limit, page * limit));
            })
            .catch((error) => {
                console.log(error.message);
            })
    }, [page, limit, varRefeshPage]);

    //PANIGATION
    const handleChangeLimit = (event) => {
        setLimit(event.target.value);
    };
    const onChangePagination = (event, value) => {
        setPage(value);
    }


    const onBtnAddOrderClick = () => {
        console.log("Nút thêm được click")
        setOpenModalAdd(true)
    }

    const onBtnEditClick = (row) => {
        console.log("Nút sửa được click")
        console.log("ID: " + row.id)
        setOpenModalEdit(true)
        setRowClicked(row)
    }

    const onBtnDeleteClick = (row) => {
        console.log("Nút xóa được click")
        console.log("ID: " + row.id)
        setOpenModalDelete(true)
        setIdDelete(row.id)
    }


    return (
        <div style={{ padding: "0 140px" }}>
            <Typography variant="h3" align="center" className="mt-5 mb-3">
                Quản lý đơn hàng
            </Typography>

            <Grid container mt={5}>
                <Grid item xs={6} mb={3}>
                    <Button className="mt-4" value="add-user"
                        onClick={onBtnAddOrderClick}
                        color="success"
                        style={{ borderRadius: 10, fontSize: "15px" }}
                        variant="contained">Add New +
                    </Button>
                </Grid>

                <Grid item xs={6}>
                    <Grid container sx={{ m: 1, minWidth: 100 }} justifyContent="flex-end">
                        <Grid item marginY={"auto"} mr={1}>
                            <InputLabel>Show</InputLabel>
                        </Grid>
                        <Grid item>
                            <Select
                                labelId="demo-simple-select-label"
                                id="demo-simple-select"
                                value={limit}
                                onChange={handleChangeLimit}
                            >
                                <MenuItem value={5}>5</MenuItem>
                                <MenuItem value={10}>10</MenuItem>
                                <MenuItem value={25}>25</MenuItem>
                                <MenuItem value={50}>50</MenuItem>
                            </Select>
                        </Grid>
                        <Grid item marginY={"auto"} ml={1}>
                            <InputLabel>orders</InputLabel>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>


            <Grid container>
                <Grid item xs={12} md={12} lg={12}>
                    <TableContainer>
                        <Table sx={{ minWidth: 650 }} aria-label="order table" className="table table-striped table-hover">
                            <TableHead>
                                <TableRow style={{ backgroundColor: "#c51162" }}>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Order ID</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Kích cỡ</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Loại Pizza</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Nước uống</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Thành tiền</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Họ và tên</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Số điện thoại</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Trạng thái</b></TableCell>
                                    <TableCell align="center" style={{ fontSize: "16px", color: "#ffffff", padding: "24px 0" }}><b>Action</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {rowOrder.map((rowOrder, index) => (
                                    <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.orderId}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.kichCo}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.loaiPizza}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.idLoaiNuocUong}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.thanhTien}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.hoTen}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.soDienThoai}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>{rowOrder.trangThai}</TableCell>
                                        <TableCell align="center" style={{ padding: "16px 0" }}>
                                            <Button value={index} onClick={() => { onBtnEditClick(rowOrder) }} variant="contained" style={{ borderRadius: 25, padding: "10px 20px", fontSize: "10px" }}>Sửa</Button>
                                            <Button value={index * index} onClick={() => { onBtnDeleteClick(rowOrder) }} style={{ borderRadius: 25, backgroundColor: "#f50057", padding: "10px 20px", fontSize: "10px" }} variant="contained">Xóa</Button>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent="flex-end">
                <Grid item >
                    <Pagination count={noPage} defaultPage={1} onChange={onChangePagination} />
                </Grid>
            </Grid>
            <ModalAddNew varRefeshPage={varRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleClose={handleClose} style={style} getData={getData} setVarRefeshPage={setVarRefeshPage} />
            <ModalEditOrder varRefeshPage={varRefeshPage} openModalEdit={openModalEdit} handleCloseEdit={handleCloseEdit}
                style={style} getData={getData} setVarRefeshPage={setVarRefeshPage} rowClicked={rowClicked}
            />
            <ModalDeleteOrder varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} style={style} openModalDelete={openModalDelete} idDelete={idDelete} handleCloseDelete={handleCloseDelete} />
        </div>
    )
}

export default OrderTable;